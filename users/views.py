from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from users.models import User, InstitutionClass, UserClass


# Create your views here.

class Home(View):
    name = "home"

    def get(self, request):
        institutes = User.objects.filter(type=1)
        context = {
            "institutes": institutes,
        }
        return render(request, "users/home.html", context)


class ViewInstitute(View):
    name = "ViewInstitute"
    template_name = "users/user_institute_view.html"

    def get(self, request, pk):
        institute = User.objects.get(type=1, id=pk)
        classes = InstitutionClass.objects.filter(user=institute)
        context = {
            "institute": institute,
            "classes": classes,
        }
        return render(request, self.template_name, context)


class ViewClass(View):
    name = "ViewClass"
    template_name = "users/user_institute_class.html"

    def get(self, request, pk):
        class_obj = InstitutionClass.objects.get(id=pk)
        if request.user.id is not None:
            if request.user.type == 1:
                is_register = -1
            else:
                user_classes = UserClass.objects.filter(user__username=request.user, user_class=class_obj)
                print(user_classes)
                if len(user_classes) > 0:
                    is_register = 1
                else:
                    is_register = 0
        else:
            is_register = 0
        context = {
            "class": class_obj,
            "is_register": is_register,
        }
        return render(request, self.template_name, context)


class RegisterClass(View):
    name = "RegisterClass"

    def post(self, request):
        if request.user.id is None:
            messages.error(request, "برای ثبت نام در کلاس باید وارد حساب کاربری خود بشوید و یا ثبت نام کنید")
            return redirect("users:login")

        class_id = request.POST.get("class_id")
        try:
            class_obj = InstitutionClass.objects.get(id=class_id)
            user_obj = User.objects.get(id=request.user.id, type=0)
        except Exception as e:
            messages.error(request, "همچین اکانت و یا کلاسی در دسترس نمیباشد")
            return redirect("users:ViewClass", class_id)
        user_class = UserClass.objects.create(user=user_obj, user_class=class_obj)
        user_class.save()
        messages.success(request, "درخواست شما با موفقیت ثبت شد")
        return redirect("users:ViewClass", class_id)


class Login(View):
    name = 'login'
    template_name = "users/login.html"

    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            if user.type == 0:
                return redirect('users:home')
            else:
                return redirect("institute:Home")
        else:
            messages.error(request, 'نام کاربری یا گذرواژه اشتباه میباشد.')
            return redirect('users:login')

    def get(self, request):
        if not request.user.is_authenticated:
            return render(request, self.template_name)
        else:
            if request.user.type == 0:
                return redirect('users:home')
            else:
                return redirect("institute:Home")


class UserRegister(View):
    name = "UserRegister"
    template_name = "users/user_register.html"

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        username = request.POST.get("username")
        phone = request.POST.get("phone")
        full_name = request.POST.get("full_name")
        password = request.POST.get("password")
        password_2 = request.POST.get("password_2")
        if password != password_2:
            messages.error(request, "رمز عبور با تکرار رمز عبور همخوانی ندارد.")
            return render(request, self.template_name)
        else:
            check_user = User.objects.filter(username=username).count()
            if check_user != 0:
                messages.error(request, "نام کاربری دیگری با این نام کاربری وجود دارد.")
                return render(request, self.template_name)
            else:
                user = User.objects.create(username=username, phone=phone, full_name=full_name, type=0)
                user.set_password(password)
                user.save()
                login(request, user)
                messages.success(request, "اکانت شما با موفقیت ایجاد شد.")
                return redirect('users:home')


def Logout(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('users:login')
    else:
        return redirect('users:login')
