from django.urls import path
from users import views

app_name = 'users'

urlpatterns = [
    path('', views.Home.as_view(), name=views.Home.name),
    path('login/', views.Login.as_view(), name=views.Login.name),
    path('register/', views.UserRegister.as_view(), name=views.UserRegister.name),
    path('<int:pk>/', views.ViewInstitute.as_view(), name=views.ViewInstitute.name),
    path('class/<int:pk>', views.ViewClass.as_view(), name=views.ViewClass.name),
    path('class/register', views.RegisterClass.as_view(), name=views.RegisterClass.name),
    path('logout/', views.Logout, name="logout"),

]
