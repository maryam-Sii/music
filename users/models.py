from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class User(AbstractUser):
    full_name = models.CharField(max_length=120)
    phone = models.CharField(max_length=120, default=None, null=True)
    type = models.BooleanField(default=0)
    logo = models.ImageField(default=None, null=True, blank=True)
    description = models.TextField(max_length=1000, default="", null=True, blank=True)
    address = models.TextField(max_length=1000, default="", null=True, blank=True)

    def __str__(self):
        return self.username

    def save(self, *args, **kwargs):
        self.username = self.username.lower()
        super(User, self).save(*args, **kwargs)


class InstitutionClass(models.Model):
    name = models.CharField(max_length=120, null=True, blank=True, default=None)
    logo = models.ImageField(null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    teacher = models.CharField(max_length=120, null=True, blank=True, default=None)
    instrument = models.CharField(max_length=120, null=True, blank=True, default=None)
    time = models.CharField(max_length=120, null=True, blank=True, default=None)

    def __str__(self):
        return self.name


class UserClass(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_class = models.ForeignKey(InstitutionClass, on_delete=models.CASCADE)


class InstituteComment(models.Model):
    institute = models.ForeignKey(User, on_delete=models.CASCADE, related_name="institute")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    comment = models.CharField(max_length=120, null=True, blank=True, default=None)
    rate = models.IntegerField(default=5)
