from django import forms
from users import models


class AddInstituteClassForm(forms.ModelForm):
    class Meta:
        model = models.InstitutionClass
        fields = "__all__"


class EditInstituteClassForm(forms.ModelForm):
    class Meta:
        model = models.InstitutionClass
        fields = ['name', 'logo', 'teacher', 'instrument', 'time']


class RegisterInstitute(forms.ModelForm):
    password_2 = forms.CharField()

    class Meta:
        model = models.User
        fields = ['username', 'full_name', 'phone', 'description', 'address', 'logo', 'type']
