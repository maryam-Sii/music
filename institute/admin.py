from django.contrib import admin
from users.models import User, InstitutionClass, UserClass

# Register your models here.
admin.site.register(User)
admin.site.register(InstitutionClass)
admin.site.register(UserClass)
