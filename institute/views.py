from django.shortcuts import render
from django.views.generic import TemplateView, View, UpdateView, DeleteView
from django.shortcuts import render, redirect
from django.contrib import messages
from institute.mixins import LoginRequiredMixin
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from institute import forms
from users.models import User, InstitutionClass, UserClass


# Create your views here.

class Home(LoginRequiredMixin, View):
    name = "Home"
    template_name = "institute/home.html"

    def get(self, request):
        classes = InstitutionClass.objects.filter(user__username=request.user)
        context = {
            'classes': classes
        }
        return render(request, self.template_name, context)


class AddClass(LoginRequiredMixin, View):
    name = "AddClass"
    form = forms.AddInstituteClassForm
    template_name = "institute/add_class.html"

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        user = User.objects.get(username=request.user)
        updated_request = request.POST.copy()
        updated_request.update({"user": user.id})
        post_form = self.form(updated_request, request.FILES)
        if post_form.is_valid():
            post_form.save()
        else:
            messages.error(request, "مشکلی پیش آمده است.")
            return render(request, self.template_name)
        messages.success(request, "کلاس با موفقیت اضافه شد")
        return redirect("institute:Home")


class EditClass(LoginRequiredMixin, UpdateView):
    model = InstitutionClass
    name = "EditClass"
    form_class = forms.EditInstituteClassForm
    template_name = "institute/edit_class.html"
    success_url = '/institute/'
    context_object_name = "class"


class DeleteClass(LoginRequiredMixin, DeleteView):
    model = InstitutionClass
    name = "DeleteClass"
    success_url = '/institute/'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class InstituteRegister(View):
    name = "InstituteRegister"
    template_name = "institute/institute_register.html"
    form = forms.RegisterInstitute

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        post_form = self.form(request.POST, request.FILES)
        if post_form.is_valid():
            password = request.POST.get("password")
            if password != post_form.cleaned_data.get("password_2"):
                messages.error(request, "رمز عبور با تکرار رمز عبور همخوانی ندارد.")
                return render(request, self.template_name)
            else:
                post_form.save()
                user = User.objects.get(username=post_form.cleaned_data.get('username'))
                user.set_password(password)
                user.save()
                login(request, user)
                messages.success(request, "اکانت شما با موفقیت ایجاد شد.")
                return redirect('institute:Home')
        else:
            print(post_form.errors)
            messages.error(request, "نام کاربری دیگری با این نام وجود دارد.")
        return render(request, self.template_name)


class InstituteClassUsers(LoginRequiredMixin, View):
    name = "InstituteClassUsers"
    template_name = "institute/institute_users.html"

    def get(self, request):
        institute_classes = InstitutionClass.objects.filter(user__username=request.user)
        users_classes = list()
        for institute_class in institute_classes:
            users = UserClass.objects.filter(user_class=institute_class)
            for usr in users:
                _dict = {
                    "username": usr.user.username,
                    "class_name": institute_class.name,
                    "class_teacher": institute_class.teacher,
                    "class_time": institute_class.time,
                }
                users_classes.append(_dict)
        context = {
            "user_classes": users_classes
        }
        return render(request, self.template_name, context)


def Logout(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('users:login')
    else:
        return redirect('users:login')
