from django.shortcuts import redirect


class LoginRequiredMixin:
    """Verify that the current user is authenticated and user is admin."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated or request.user.type == 0:
            return redirect("users:login")
        return super().dispatch(request, *args, **kwargs)
