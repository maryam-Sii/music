from django.urls import path
from institute import views

app_name = 'users'

urlpatterns = [
    path('', views.Home.as_view(), name=views.Home.name),
    path('add/', views.AddClass.as_view(), name=views.AddClass.name),
    path('register/', views.InstituteRegister.as_view(), name=views.InstituteRegister.name),
    path('edit/<int:pk>', views.EditClass.as_view(), name=views.EditClass.name),
    path('delete/<int:pk>', views.DeleteClass.as_view(), name=views.DeleteClass.name),
    path('users/', views.InstituteClassUsers.as_view(), name=views.InstituteClassUsers.name),
    path('logout/', views.Logout, name="logout"),


]
